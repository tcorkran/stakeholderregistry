#!/usr/bin/env perl

use strict;
use warnings;

use Catalyst::Test 'StakeholderRegister';
use HTTP::Request::Common;
use JSON;
use LWP::UserAgent;
use Test::MockModule;
use Test::Most;

subtest 'POST /project' => sub {  
    my $mock_return = {
        id        => 1,
        name      => 'Chat System',
        manager   => 'Bob Smith',
        sponsor   => 'Alfred Baxter',
        objective => 'Chats',
        created   => '2016-04-15 14:47:10',
        updated   => '2016-04-15 14:47:10',              
    };
    
    my $mock_schema = Test::MockModule->new('StakeholderRegister::Schema::ResultSet::Project');
    $mock_schema->mock('add', sub { return $mock_return } ); 
    
    #my $ua = LWP::UserAgent->new;
    
    #my $request = HTTP::Request->new( POST => '/project' );
    #$request->header( 'content-type' => 'application/json' );
           
    #my $post_data = '{ "name": "Chat System", "manager": "Bob Smith", "sponsor": "Alfred Baxter" }';       
           
    #$request->content($post_data);
    
    #my $response = $ua->request($request);
    
    my $response = request POST '/project',
        Content  => [
            name         => 'Chat System',
            manager      => 'Bob Smith',
            sponsor      => 'Alfred Baxter',
            objective    => 'Chats',
        ];
        
    ok( $response->is_success, 'Request Successful');
       
    if ( ok( $response->header('content-type'), 'Content type is JSON') ) {
        my $decoded_json = decode_json( $response->decoded_content );
        
        is_deeply( $decoded_json, $mock_return, 'Returned Body Correct');
    }      
};