#!/usr/bin/env perl

use strict;
use warnings;

use Catalyst::Test 'StakeholderRegister';
use HTTP::Request::Common;
use JSON;
use Test::MockModule;
use Test::Most;

subtest 'GET /project' => sub {  
    my $mock_value = [
        {
            id      => 1,
            name    => 'Chat System',
            manager => 'Bob Smith',
            sponsor => 'Alfred Baxter',
            created => '2016-04-15 14:47:10',
            updated => '2016-04-15 19:57:18',              
        },
        {
            id      => 2,
            name    => 'Ticket System',
            manager => 'Peter Parker',
            sponsor => 'Bruce Wayne',
            created => '2016-04-16 11:37:10',
            updated => '2016-04-16 12:17:16',              
        },
    ];
    
    my $mock_schema = Test::MockModule->new('StakeholderRegister::Schema::ResultSet::Project');
    $mock_schema->mock('retrieve', sub { return $mock_value } ); 
        
    my $expected_payload = {
        count  => scalar( @{ $mock_value } ),
        value  => $mock_value,
    };
    
    my $response = request GET '/project?content-type=application/json';
        
    ok( $response->is_success, 'Request Successful');
       
    if ( ok( $response->header('content-type'), 'Content type is JSON') ) {
        my $decoded_json = decode_json( $response->decoded_content );
        
        is_deeply( $decoded_json, $expected_payload , 'Returned Body Correct');
    }      
};

subtest 'GET /project/:id' => sub {
    my $mock_value = [
        {
            id      => 1,
            name    => 'Chat System',
            manager => 'Bob Smith',
            sponsor => 'Alfred Baxter',
            created => '2016-04-15 14:47:10',
            updated => '2016-04-15 19:57:18',              
        }
    ];

    my $mock_schema = Test::MockModule->new('StakeholderRegister::Schema::ResultSet::Project');
    $mock_schema->mock('retrieve', sub { return $mock_value } );    

    my $expected_payload = {
        count  => scalar( @{ $mock_value } ),
        value  => $mock_value,
    };
    
    my $response = request GET '/project/1?content-type=application/json';
        
    ok( $response->is_success, 'Request Successful');
       
    if ( ok( $response->header('content-type'), 'Content type is JSON') ) {
        my $decoded_json = decode_json( $response->decoded_content );
        
        is_deeply( $decoded_json, $expected_payload , 'Returned Body Correct');
    }    
};

done_testing();