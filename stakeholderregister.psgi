use strict;
use warnings;

use StakeholderRegister;

my $app = StakeholderRegister->apply_default_middlewares(StakeholderRegister->psgi_app);
$app;

