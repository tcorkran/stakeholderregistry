--
-- Create a very simple database to hold stakeholder information
--

PRAGMA foreign_keys = ON;
CREATE TABLE project (
    id              INTEGER PRIMARY KEY NOT NULL,
    name            TEXT NOT NULL,
    objective       TEXT,
    manager         TEXT NOT NULL,
    sponsor         TEXT NOT NULL,
    created         TIMESTAMP NOT NULL,
    updated         TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- 'stakeholder'
CREATE TABLE stakeholder (
    id              INTEGER PRIMARY KEY NOT NULL,
    name            TEXT NOT NULL,
    title           TEXT,
    org_role        TEXT,
    phone           TEXT,
    email           TEXT
);

-- 'project_stakeholder'
CREATE TABLE project_stakeholder (
    id              INTEGER PRIMARY KEY NOT NULL,
    stakeholder_id  INTEGER,
    project_id      INTEGER,
    description     TEXT,
    status          TEXT,
    category        TEXT,
    interest        REAL,
    influence       REAL,
    strategy        TEXT,
    created         TIMESTAMP NOT NULL,
    updated         TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    FOREIGN KEY(stakeholder_id) REFERENCES stakeholder(id),
    FOREIGN KEY(project_id) REFERENCES project(id)
);

