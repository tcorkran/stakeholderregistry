package StakeholderRegister::Controller::Projects;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller::REST'; }

sub projects : Local : ActionClass('REST') { }

=head1 NAME

StakeholderRegister::Controller::Projects - Catalyst RESTful Controller

=head1 DESCRIPTION

Catalyst RESTful Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->response->body('Matched StakeholderRegister::Controller::Projects in Projects.');
}

=head2 GET Requests

=cut

# Answer Get requests to "Projects"
sub projects_GET {
    my ($self, $c ) = @_;
    
    # Return a 200 OK, with the data in payload serialized
    $self->status_ok(
        $c,
        data => {
            # put stuff here
        },
    );
}

=head2 PUT Requests

=cut

# Answer Put requests to "Projects"
sub projects_PUT {
    my ($self, $c ) = @_;
}

=encoding utf8

=head1 AUTHOR

Thomas,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
