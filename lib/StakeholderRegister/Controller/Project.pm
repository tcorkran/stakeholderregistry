package StakeholderRegister::Controller::Project;

use strict;
use warnings;
use Moose;
use POSIX;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller::REST'; }

sub project : Path('/project') : Local : ActionClass('REST') { }

=head1 NAME

StakeholderRegister::Controller::Project - Catalyst RESTful Controller

=head1 DESCRIPTION

Catalyst RESTful Controller.

=head1 METHODS

=cut

=head2 GET Requests

=cut

# Answer Get requests to "Projects"
sub project_GET {
    my ( $self, $c, $id ) = @_;

    #my $id = $c->req->data->{id};
    
    my $projects = $c->model( 'DB::Project' )->retrieve( $id );
     
    # Return a 200 OK, with the data in payload serialized
    $self->status_ok(
        $c,
        entity => {
            count => scalar( @{ $projects } ),
            value => $projects,
        }
    );
}

=head2 POST Requests

=cut

# Answer Post requests to "Projects"
sub project_POST {
    my ( $self, $c ) = @_;

    # Our location as this is a processed request
    my $location  = $c->req->uri->as_string;
    
    unless ( defined ( $c->req->data ) ) {
        return $self->status_bad_request(
            $c,
            message => 'You must provide data for the project!',
        );
    }
    
    # Retrieve the values from the form
    my $name      = $c->req->data->{name};
    my $manager   = $c->req->data->{manager};
    my $objective = $c->req->data->{objective};
    my $sponsor   = $c->req->data->{sponsor};
    
    # Check required fields
    if ( !defined( $name ) ) {
        return $self->status_bad_request(
            $c,
            message => 'You must provide a name for the project!',
        );
    }
    
    if ( !defined( $manager ) ) {
        return $self->status_bad_request(
            $c,
            message => 'You must provide a manager for the project!',
        );
    }
    
    if ( !defined( $sponsor ) ) {
        return $self->status_bad_request(
            $c,
            message => 'You must provide a sponsor for the project!',
        );
    }
    
    # Data we will save to the DB
    my $params = {
        name      => $name,
        manager   => $manager,
        objective => $objective,
        sponsor   => $sponsor,
        created   => strftime( "%F %T", localtime() ),        
    };
    
    # Create the project
    my $project = $c->model( 'DB::Project' )->add( $params );
            
    if ( $project ) {
        $self->status_created(
            $c,
            location => $location . '/' . $project->{id},
            entity   => $project,
        );            
    }
    else {
        $self->status_ok(
            $c,
            entity => {
                message => 'Unable to create project!',
            },
        ); 
    }
}

=head2 PUT Requests

=cut

# Answer Put requests to "Projects"
sub project_PUT {
    my ( $self, $c, $id ) = @_;
    
    my $location = $c->req->uri->as_string;
    my $params;
    
    $params->{name} = $c->req->data->{name} if $c->req->data->{name};
    $params->{manager} = $c->req->data->{manager} if $c->req->data->{manager};
    $params->{objective} = $c->req->data->{objective} if $c->req->data->{objective};
    $params->{sponsor} = $c->req->data->{sponsor} if $c->req->data->{sponsor};
    
    my $project = $c->model( 'DB::Project' )->edit( $id, $params );
    
    if ( $project ) {
        $self->status_accepted(
            $c,
            location => $location . '/' . $project->{id},
            entity => $project
        );
    }
    else {
        $self->status_not_found(
            $c,
            message => 'Project ID: ' . $id . ' not found!',
        );
    }  
}

=head2 DELETE Requests

=cut

# Answer Delete requests to "Projects"
sub project_DELETE {
    my ( $self, $c, $id ) = @_;
    
    my $result = $c->model( 'DB::Project' )->del( $id );
    
    if ( $result ) {
        $self->status_accepted(
            $c,
            location => $c->req->uri->as_string,
            entity => {},
        );
    }
    else {
        $self->status_not_found(
            $c,
            message => 'Project ID: ' . $id . ' not found!',
        );
    }
}

=encoding utf8

=head1 AUTHOR

Thomas,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

# Uncomment and/or change if you wish to never receive 415 Unsupported Tpe when
# unsupported type or missing type is given.
#__PACKAGE__->config(default => 'application/json');

__PACKAGE__->meta->make_immutable;

1;
