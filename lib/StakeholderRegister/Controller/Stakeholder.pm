package StakeholderRegister::Controller::Stakeholder;

use strict;
use warnings;
use Moose;
use POSIX;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller::REST'; }

sub stakeholder : Path('/stakeholder') : Local : ActionClass('REST') { }

=head1 NAME

StakeholderRegister::Controller::Stakeholder - Catalyst RESTful Controller

=head1 DESCRIPTION

Catalyst RESTful Controller.

=head1 METHODS

=cut

=head2 GET Requests

=cut

# Answer Get requests to "Stakeholder"
sub stakeholder_GET {
    my ( $self, $c, $id ) = @_;

    my $stakeholders = $c->model( 'DB::Stakeholder' )->retrieve( $id );
     
    # Return a 200 OK, with the data in payload serialized
    $self->status_ok(
        $c,
        entity => {
            count => scalar( @{ $stakeholders } ),
            value => $stakeholders,
        }
    );
}

=head2 POST Requests

=cut

# Answer Post requests to "Stakeholders"
sub stakeholder_POST {
    my ( $self, $c ) = @_;

    # Our location as this is a processed request
    my $location  = $c->req->uri->as_string;
    
    # Retrieve the values from the form
    my $name     = $c->req->data->{name};
    my $title    = $c->req->data->{title};
    my $org_role = $c->req->data->{orgRole};
    my $phone    = $c->req->data->{phone};
    my $email    = $c->req->data->{email};
    
    # Check required fields
    if ( !defined( $name ) ) {
        return $self->status_bad_request(
            $c,
            message => 'You must provide a name for the stakeholder!',
        );
    }
     
    # Data we will save to the DB
    my $params = {
        name     => $name,
        title    => $title,
        org_role => $org_role,
        phone    => $phone,
        email    => $email,
        created  => strftime( "%F %T", localtime() ),        
    };
    
    # Create the project
    my $stakeholder = $c->model( 'DB::Stakeholder' )->add( $params );
      
    # Response 
    my $response = {
        id   => $stakeholder->id,
        name => $stakeholder->name,
        self => $location . '/' . $stakeholder->id,
    };
    
    if ( $stakeholder ) {
        $self->status_created(
            $c,
            location => $c->req->uri->as_string,
            entity => $response,
        );            
    }
    else {
        $self->status_ok(
            $c,
            entity => $response,
        ); 
    }
}

=head2 PUT Requests

=cut

# Answer Put requests to "Stakeholders"
sub stakeholder_PUT {
    my ( $self, $c, $id ) = @_;
    
    my $location = $c->req->uri->as_string;
    my $params;
    
    $params->{name}    = $c->req->data->{name} if $c->req->data->{name};
    $params->{title}   = $c->req->data->{title} if $c->req->data->{title};
    $params->{orgRole} = $c->req->data->{orgRole} if $c->req->data->{orgRole};
    $params->{phone}   = $c->req->data->{phone} if $c->req->data->{phone};
    $params->{email}   = $c->req->data->{email} if $c->req->data->{email};
    
    my $stakeholder = $c->model( 'DB::Stakeholder' )->edit( $id, $params );
    
    if ( $stakeholder ) {
        $self->status_accepted(
            $c,
            location => $location,
            entity => {
                id   => $stakeholder->id,
                name => $stakeholder->name,
                self => $location,
            }
        );
    }
    else {
        $self->status_not_found(
            $c,
            message => 'Stakeholder ID: ' . $id . ' not found!',
        );
    }  
}

=head2 DELETE Requests

=cut

# Answer Delete requests to "Stakeholders"
sub stakeholder_DELETE {
    my ( $self, $c, $id ) = @_;
    
    my $result = $c->model( 'DB::Stakeholder' )->del( $id );
    
    if ( $result ) {
        $self->status_accepted(
            $c,
            location => $c->req->uri->as_string,
            entity => {},
        );
    }
    else {
        $self->status_not_found(
            $c,
            message => 'Stakeholder ID: ' . $id . ' not found!',
        );
    }
}

=encoding utf8

=head1 AUTHOR

Thomas,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

# Uncomment and/or change if you wish to never receive 415 Unsupported Type when
# unsupported type or missing type is given.
#__PACKAGE__->config(default => 'application/json');

__PACKAGE__->meta->make_immutable;

1;
