package StakeholderRegister::Controller::ProjectStakeholder;

use strict;
use warnings;
use Moose;
use POSIX;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller::REST'; }

sub ps : Path('/projectstakeholder') : Local : ActionClass('REST') { }

=head1 NAME

StakeholderRegister::Controller::ProjectStakeholder - Catalyst RESTful Controller

=head1 DESCRIPTION

Catalyst RESTful Controller.

=head1 METHODS

=cut

=head2 GET Requests

=cut

# Answer Get requests to "ProjectStakeholders"
sub ps_GET {
    my ( $self, $c ) = @_;

    my $project_stakeholders = $c->model( 'DB::ProjectStakeholder' )->retrieve;
     
    # Return a 200 OK, with the data in payload serialized
    $self->status_ok(
        $c,
        entity => {
            count => scalar( @{ $project_stakeholders } ),
            value => $project_stakeholders,
        }
    );
}

=head2 POST Requests

=cut

# Answer Post requests to "ProjectStakeholders"
sub ps_POST {
    my ( $self, $c ) = @_;

    # Our location as this is a processed request
    my $location  = $c->req->uri->as_string;
   
    # Retrieve the values from the form
    my $stakeholder_id = $c->req->data->{stakeholderId};
    my $project_id     = $c->req->data->{projectId};
    my $description    = $c->req->data->{description};
    my $status         = $c->req->data->{status};
    my $category       = $c->req->data->{category};
    my $interest       = $c->req->data->{interest};
    my $influence      = $c->req->data->{influence};
    my $strategy       = $c->req->data->{strategy};
    
    # Check required fields
    if ( !defined( $stakeholder_id ) ) {
        return $self->status_bad_request(
            $c,
            message => 'You must provide a stakeholderId for the project stakeholder!',
        );
    }
    
    if ( !defined( $project_id ) ) {
        return $self->status_bad_request(
            $c,
            message => 'You must provide a projectId for the project stakeholder!',
        );
    }
       
    # Data we will save to the DB
    my $params = {
        stakeholder_id => $stakeholder_id,
        project_id     => $project_id,
        description    => $description,
        status         => $status,
        category       => $category,
        interest       => $interest,
        influence      => $influence,
        strategy       => $strategy,
        created   => strftime( "%F %T", localtime() ),        
    };
    
    # Create the project
    my $project_stakeholder = $c->model( 'DB::ProjectStakeholder' )->add( $params );
      
    # Response 
    my $response = {
        id   => $project_stakeholder->id,
        #name => $project_stakeholder->name,
        self => $location . '/' . $project_stakeholder->id,
    };
    
    if ( $project_stakeholder ) {
        $self->status_created(
            $c,
            location => $c->req->uri->as_string,
            entity => $response,
        );            
    }
    else {
        $self->status_ok(
            $c,
            entity => $response,
        ); 
    }
}

=head2 PUT Requests

=cut

# Answer Put requests to "ProjectStakeholders"
sub ps_PUT {
    my ( $self, $c, $id ) = @_;
    
    my $location = $c->req->uri->as_string;
    my $params;
   
    $params->{stakeholderId} = $c->req->data->{stakeholderId} if $c->req->data->{stakeholderId};
    $params->{projectId}     = $c->req->data->{projectId} if $c->req->data->{projectId};
    $params->{description}   = $c->req->data->{description} if $c->req->data->{description};
    $params->{status}        = $c->req->data->{status} if $c->req->data->{status};
    $params->{category}      = $c->req->data->{category} if $c->req->data->{category};
    $params->{interest}      = $c->req->data->{interest} if $c->req->data->{interest};
    $params->{influence}     = $c->req->data->{influence} if $c->req->data->{influence};
    $params->{strategy}      = $c->req->data->{strategy} if $c->req->data->{strategy};
    
    my $project_stakeholder = $c->model( 'DB::Project' )->edit( $id, $params );
    
    if ( $project_stakeholder ) {
        $self->status_accepted(
            $c,
            location => $location,
            entity => {
                id   => $project_stakeholder->id,
                self => $location,
            }
        );
    }
    else {
        $self->status_not_found(
            $c,
            message => 'Project Stakeholder ID: ' . $id . ' not found!',
        );
    }  
}

=head2 DELETE Requests

=cut

# Answer Delete requests to "Projects"
sub ps_DELETE {
    my ( $self, $c, $id ) = @_;
    
    my $result = $c->model( 'DB::ProjectStakeholder' )->del( $id );
    
    if ( $result ) {
        $self->status_accepted(
            $c,
            location => $c->req->uri->as_string,
            entity => {},
        );
    }
    else {
        $self->status_not_found(
            $c,
            message => 'Project Stakeholder ID: ' . $id . ' not found!',
        );
    }
}

=encoding utf8

=head1 AUTHOR

Thomas,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

# Uncomment and/or change if you wish to never receive 415 Unsupported Tpe when
# unsupported type or missing type is given.
#__PACKAGE__->config(default => 'application/json');

__PACKAGE__->meta->make_immutable;

1;
