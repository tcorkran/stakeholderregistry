use utf8;
package StakeholderRegister::Schema::Result::ProjectStakeholder;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

StakeholderRegister::Schema::Result::ProjectStakeholder

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 TABLE: C<project_stakeholder>

=cut

__PACKAGE__->table("project_stakeholder");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 stakeholder_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 project_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 status

  data_type: 'text'
  is_nullable: 1

=head2 category

  data_type: 'text'
  is_nullable: 1

=head2 interest

  data_type: 'real'
  is_nullable: 1

=head2 influence

  data_type: 'real'
  is_nullable: 1

=head2 strategy

  data_type: 'text'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 0

=head2 updated

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "stakeholder_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "project_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "status",
  { data_type => "text", is_nullable => 1 },
  "category",
  { data_type => "text", is_nullable => 1 },
  "interest",
  { data_type => "real", is_nullable => 1 },
  "influence",
  { data_type => "real", is_nullable => 1 },
  "strategy",
  { data_type => "text", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 0 },
  "updated",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 project

Type: belongs_to

Related object: L<StakeholderRegister::Schema::Result::Project>

=cut

__PACKAGE__->belongs_to(
  "project",
  "StakeholderRegister::Schema::Result::Project",
  { id => "project_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);

=head2 stakeholder

Type: belongs_to

Related object: L<StakeholderRegister::Schema::Result::Stakeholder>

=cut

__PACKAGE__->belongs_to(
  "stakeholder",
  "StakeholderRegister::Schema::Result::Stakeholder",
  { id => "stakeholder_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);


# Created by DBIx::Class::Schema::Loader v0.07045 @ 2016-04-12 23:11:11
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:2DP1ZDX4dhUCCB6FkLlZOg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
