package StakeholderRegister::Schema::ResultSet::Stakeholder;

use strict;
use warnings;
use base 'DBIx::Class::ResultSet';

sub add {
    my ( $self, $params ) = @_;
    
    my $stakeholder = $self->create( $params );
    
    return $stakeholder;
}

sub edit {
    my ( $self, $id, $params ) = @_;
    
    my $stakeholder = $self->find( $id );
    
    if ( $stakeholder ) {
        $stakeholder->update( $params );
        
        return $stakeholder;
    }
    else {
        return 0;
    }
}

sub del {
    my ( $self, $id ) = @_;
    
    my $stakeholder = $self->find( $id );
      
    if ( $stakeholder ) {
        $stakeholder->delete;
        
        return 1;
    }
    else {
        return 0;
    }
}

sub retrieve {
    my ( $self, $id ) = @_;
    my $stakeholders;
    
    if ( $id ) {
        $stakeholders = $self->search( { id => $id } );
    }
    else {
        $stakeholders = $self->search( undef, { order_by => 'id' } );
    }
    
    my @data;
    while ( my $s = $stakeholders->next ) {
        my %row = $s->get_columns(); 
        push( @data, \%row );
    }
    
    return \@data;
}

1;