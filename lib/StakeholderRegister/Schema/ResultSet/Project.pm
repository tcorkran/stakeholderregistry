package StakeholderRegister::Schema::ResultSet::Project;

use strict;
use warnings;
use base 'DBIx::Class::ResultSet';

sub add {
    my ( $self, $params ) = @_;
    
    my $project = $self->create( $params );
    
    if ( $project ) {
        my %data = $project->get_columns();
        return \%data;
    }
    else {
        return 0;
    } 
}

sub edit {
    my ( $self, $id, $params ) = @_;
    
    my $project = $self->find( $id );
    
    if ( $project ) {
        $project->update( $params );
        
        my %data = $project->get_columns();
        return \%data;
    }
    else {
        return 0;
    }
}

sub del {
    my ( $self, $id ) = @_;
    
    my $project = $self->find( $id );
      
    if ( $project ) {
        $project->delete;
        
        return 1;
    }
    else {
        return 0;
    }
}

sub retrieve {
    my ( $self, $id ) = @_;
    my $projects;
    
    if ( $id ) {
        $projects = $self->search( { id => $id } );
    }
    else {
        $projects = $self->search( undef, { order_by => 'id' } );
    }
    
    my @data;
    while ( my $p = $projects->next ) {
        my %row = $p->get_columns(); 
        push( @data, \%row );
    }
    
    return \@data;
}



1;