package StakeholderRegister::Schema::ResultSet::ProjectStakeholder;

use strict;
use warnings;
use base 'DBIx::Class::ResultSet';

sub add {
    my ( $self, $params ) = @_;
    
    my $project_stakeholder = $self->create( $params );
    
    return $project_stakeholder;
}

sub edit {
    my ( $self, $id, $params ) = @_;
    
    my $project_stakeholder = $self->find( $id );
    
    if ( $project_stakeholder ) {
        $project_stakeholder->update( $params );
        
        return $project_stakeholder;
    }
    else {
        return 0;
    }
}

sub del {
    my ( $self, $id ) = @_;
    
    my $project_stakeholder = $self->find( $id );
      
    if ( $project_stakeholder ) {
        $project_stakeholder->delete;
        
        return 1;
    }
    else {
        return 0;
    }
}

sub retrieve {
    my ( $self, $id ) = @_;
    my $project_stakeholders;
    
    if ( $id ) {
        $project_stakeholders = $self->search( { id => $id } );
    }
    else {
        $project_stakeholders = $self->search( undef, { order_by => 'id' } );
    }
    
    my @data;
    while ( my $p = $project_stakeholders->next ) {
        my %row = $p->get_columns(); 
        push( @data, \%row );
    }
    
    return \@data;
}

1;